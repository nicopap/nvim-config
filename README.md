# Nvim config

This is part of my public dotfiles repositories. I discovered fennel, then
aniseed, and decided to start using it to configure neovim. Now I can use a
language I understand to extend my editor!


## Deployment

I use [`stow`](https://www.gnu.org/software/stow/) for dotfile management.

Once `stow` and `git` are installed on a computer, and that computer has
internet access, I just run:

```sh
git clone https://gitlab.com/nicopap/nvim-config.git

stow --dotfiles --ignore='README\.md|\.git' -t ~ -v3 nvim-config
```

This is not all! You need to install [`dein`](https://github.com/Shougo/dein.vim)
and run the first install in neovim with `:call dein#install()`.

The install instructions are available in the `dein` README, but for posterity:

```sh
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
sh ./installer.sh ~/.cache/dein
nvim -c ":dein#install()"
rm installer.sh
```


## Subtreeing

I have a (private) general dotfiles repository that I use when setting up a new
machine. This repo is simply `subtree`-ed (see `man git-subtree`) in the
general repo.


## Errors

So now, I'm an twit, and I don't even use the great module system that aniseed
provides. **If you use this dotfiles config, you'll get errors whenever you
start neovim**. This is because aniseed tries to load the `my-macros.fnl`
file which defines macros I use in other modules. It cannot do so because the
macros are only evaluated if imported with `require-macros`. This results in
the error message that shows up every time I open neovim.

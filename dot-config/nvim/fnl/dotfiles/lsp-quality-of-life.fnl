(require-macros "dotfiles.my-macros")
(local nvim (require "aniseed.nvim"))
(local a (require "aniseed.core"))

(macros { ".#" (fn [key tbl] `(. ,tbl ,key)) })

(fn to-vim-quickfix-item [default-uri item]
  "Convert an lsp diagnostic message into a vim quickfix list item
  (`:h setqflist`)
  Uses default-uri if the :uri field is not available."
  (let [severities ["Error" "Warning" "Information" "Hint"]
        kind (. severities item.severity)]
    { :filename (vim.uri_to_fname (or item.uri default-uri))
      :lnum (+ item.range.start.line 1)
      :col (+ item.range.start.character 1)
      :kind kind
      :text (.. "[" kind "] " item.message)
    }))

(fn extends-callback [callbacks method after-callback]
  (local previous-callback (. callbacks method))
  (tset callbacks method
    (fn [err method result client-id]
      (previous-callback err method result client-id)
      (after-callback result))))


{ :pop-up
  (fn []
    "display this line's error or hover tooltip if no errors
    TODO: cycle error-message/tooltip with repeated invocations (probably needs
          to check somewhere if we have an error window open)"
    (let [this-bufnr (vim.api.nvim_get_current_buf)
          this-lnum (vim.fn.line ".")
          quick-fixes (vim.fn.getqflist)
          is-current-line? #(and (= $.bufnr this-bufnr) (= $.lnum this-lnum))]
      (if (a.some is-current-line? quick-fixes)
        (vim.lsp.util.show_line_diagnostics)
        (vim.lsp.buf.hover))))

  :get-errors
  (fn [callbacks]
    "Add hook to publishDiagnostics lsp messages to fill the quickfix list.
    Taking care to avoid duplicates, with the :context argument"
    (extends-callback callbacks "textDocument/publishDiagnostics"
      (fn [result]
        (-?>> result
          (.# :diagnostics)
          (a.map (partial to-vim-quickfix-item result.uri))
          (#(vim.fn.setqflist {} "r"
            { :title "Workspace Diagnostics"
              :context [ "errlist" result.uri ]
              :items $
            }))))))
}

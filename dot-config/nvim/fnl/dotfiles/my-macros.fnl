(fn nvfn [fun-name ...] `(vim.api.nvim_call_function ,fun-name [,...]))
(fn nvcmd [...] `(vim.api.nvim_command ,...))
(fn nvset [variable value] `(vim.api.nvim_set_var ,variable ,value))

(fn l   [char] `(eval-compiler (.. "\"<Leader>" ,char "\"")))
(fn ctl [char] `(eval-compiler (.. "\"<C-" ,char ">\"")))
(fn shf [char] `(eval-compiler (.. "\"<S-" ,char ">\"")))
(fn alt [char] `(eval-compiler (.. "\"<M-" ,char ">\"")))

(fn map  [...]  `(nvcmd (table.concat ["noremap" ,...] " ")))
(fn mapi [...] `(nvcmd (table.concat ["inoremap" ,...] " ")))
(fn mapo [...] `(nvcmd (table.concat ["onoremap" ,...] " ")))
(fn mapn [...] `(nvcmd (table.concat ["nnoremap" ,...] " ")))
{ : nvfn
  : nvcmd
  : nvset
  : l
  : ctl
  : shf
  : alt
  : map
  : mapi
  : mapo
  : mapn
}

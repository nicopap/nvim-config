(require-macros "dotfiles.my-macros")
(local lsp (require "nvim_lsp"))
(local mappings (require "dotfiles.language-mapping"))
(local ncm2 (require "ncm2"))
(local colorizer (require "colorizer"))
(local my-lsp (require "dotfiles.lsp-quality-of-life"))
; TODO explore possibility to use MUcomplete with neosnippet
; for better nvim-lsp completion integration
; https://github.com/vigoux/dotfiles/
; https://github.com/lifepillar/vim-mucomplete
; Or maybe implement a way dumber completion:
; https://github.com/neovim/nvim-lsp/issues/130#issuecomment-591917001

(let [no-separator { :left "" :right "" }]
  (nvset :lightline
    { :colorscheme :onedark
      :component { :readonly "%{&readonly?\"\":\"\"}" }
      :separator no-separator
      :tabline_separator no-separator
      :tabline_subseparator no-separator
      :active
        { :left [[:mode :paste] [:readonly :relativepath :modified]]
          :right [[:lineinfo] [:filetype]]
        }
    }))

(nvset "polyglot_disabled" [:css :yaml :markdown :haskell :elm])
(nvset "vim_markdown_math" 1)
(nvset "vim_markdown_fenced_languages" ["Haskell=elm"])
(nvset "autofmt_autosave" 1)
(nvset "rust_recommended_style" 0)

(nvset "UltiSnipsSnippetDirectories" ["UltiSnips" "extrasnips"])
(nvset "UltiSnipsExpandTrigger" "<Plug>(ultisnips_expand)")
(nvset "UltiSnipsJumpForwardTrigger" "<M-j>")
(nvset "UltiSnipsJumpBackwardTrigger" "<M-k>")
(mapi "<expr> <CR> (pumvisible() ? \"\\<C-y>\\<CR>\" : \"\\<CR>\")")
(mapi "<expr> <Tab> pumvisible() ? \"\\<C-n>\" : \"\\<Tab>\"")
(mapi "<expr>" (shf :Tab) "pumvisible() ? \"\\<C-p>\" : \"\\<S-Tab>\"")
(mapi "<silent>" (alt :j) "<C-r>=ncm2_ultisnips#expand_or(\"\\<Plug>(ultisnips_expand)\")<CR>")

(nvcmd "set completeopt=noinsert,menuone,noselect")
(nvcmd "set conceallevel=2") ; Less noisy logging
(nvcmd "set shortmess+=c")
(nvcmd "set termguicolors")
(nvset "onedark_terminal_italics" 1)
(nvcmd "colorscheme onedark")
(nvcmd "set nocursorline")
(nvcmd "set colorcolumn=80")
(nvcmd "set noshowmode") ; Do not show -- INSERT -- in command bar
(nvcmd "set inccommand=split") ; Live feedback for substitution
(nvcmd "set lazyredraw") ; Doesn't visually play actions in middle of macros
(nvcmd "set title") ; Terminal name
(nvcmd "set backspace=indent,eol,start")
(nvcmd "filetype plugin indent on")
(nvcmd "set expandtab") ; TAB inserts 4 spaces
(nvcmd "set tabstop=4") ; Number of visual spaces per TAB
(nvcmd "set shiftwidth=4")
(nvcmd "set foldmethod=indent")
(nvcmd "set foldlevelstart=30")
(nvcmd "set grepprg=rg\\ --vimgrep") ; Use ripgrep for grepping
(nvcmd "set signcolumn=no") ; Prevents flickering sign gutter

(nvcmd "let mapleader=' '")
(map "<Space>" "<nop>")

; shortcuts
(mapi (ctl :l) (ctl :c))
(map (l "<Space>") :zz)
(map (l :f)        :za)
(map (l :-) ":tabe ")
(map :-     ":")
(map ","    ";")
(map ";"    ",")
(map :ZU ":w<CR>")
(map (l :s) "<C-\\><C-n><C-w>j:q<CR>")
(map (l :d) "<C-\\><C-n><C-w>k:q<CR>")

; surrounding function
(mapo "lf" ":<C-u>normal 1[%hvb<cr>")

; tab navigation
(map "<Tab>"    "gt")
(map (shf :Tab) "gT")

; go to definition, back to previous position
(map "<CR>"        (ctl "]"))
(map "<BackSpace>" (ctl :t))

; pasting
(map  (l :Y) "\"*y")
(map  (l :y) "\"+y")
(mapn (l :P) "\"*p")
(mapn (l :p) "\"+p")

; navigate quickfix/error list (see fnl/moves.fnl for details)
(map (ctl :q) "<cmd>lua move_c_list()<CR>")
(map (shf :q) "<cmd>lua move_l_list()<CR>")

(global move_l_list (fn []
  (let [{: idx : size} (vim.fn.getloclist {:idx 0 :size 0})]
    (nvcmd (if (= idx size) :lfirst :lnext)))))

(global move_c_list (fn []
  (let [{: idx : size} (vim.fn.getqflist {:idx 0 :size 0})]
    (nvcmd (if (= idx size) :cfirst :cnext)))))

; highlight, relative line n° gutter, absolute line n° gutter
(fn func-key-map [key action]
  (map  key (.. ":set "            action "! " action "?<CR>"))
  (mapi key (.. "<C-\\><C-O>:set " action "! " action "?<CR>")))

(func-key-map "<F3>" "hlsearch")
(func-key-map "<F4>" "rnu")
(func-key-map "<F5>" "nu")

; lsp
(fn keymap-buf [buf-nr from to]
  (vim.api.nvim_buf_set_keymap buf-nr :n from to { :silent true :noremap true }))

(fn lsp-bindings [buf-nr]
  (global err_or_hover_details my-lsp.pop-up)
  (local keymap (partial keymap-buf buf-nr))
  (keymap :K "<cmd>lua err_or_hover_details()<CR>")
  (keymap "<Return>" "<cmd>lua vim.lsp.buf.definition()<CR>")
  (keymap :gs "<cmd>lua vim.lsp.buf.references()<CR>")
  (keymap :gS "<cmd>lua vim.lsp.buf.document_symbol()<CR>")
  (keymap "<BackSpace>" "<C-o>"))

(fn rust-bindings [buf-nr] (lsp-bindings buf-nr))

(my-lsp.get-errors vim.lsp.callbacks)
(lsp.rust_analyzer.setup
  { :log_level vim.lsp.protocol.MessageType.Log
    :on_init ncm2.register_lsp_source
  })


; autocmds
(mappings.on-enter-lang { :rust rust-bindings })

(colorizer.setup) ; sets up autocmds

(mappings.on :ColorScheme "*"
  #(do
    (local extend vim.fn.onedark#extend_highlight)
    (extend :Comment    { :fg { :gui "#8ba2bf" } })
    (extend :Error      { :fg { :gui "#282c34" } :bg { :gui "#e06c75" } })
    (extend :Identifier { :fg { :gui "#e06c75" } })
    (extend :Keyword    { :fg { :gui "#e06c75" } })
    (extend :Function   { :fg { :gui "#61afef" } })))
(nvcmd "autocmd VimEnter bash-fc.* setf sh")
(nvcmd "autocmd VimEnter .bash_history setf sh")
(nvcmd "autocmd FileType * call ncm2#enable_for_buffer()")
{}

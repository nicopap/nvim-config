; TODO: by-language mapping (look into buffer-specific map possible?)

(global custom_autoloads {})
(require-macros "dotfiles.my-macros")
(fn on-enter-lang [lang-mapping]
  "Given a `lang-mapping`, a table of lang->procedure, registers the execution
  of the provided procedure at VimEnter lang, giving to procedure the current
  bufnbr."
  (each [lang mapping (pairs lang-mapping)]
    (tset custom_autoloads lang mapping)
    (nvcmd (.. "autocmd FileType " lang " lua custom_autoloads." lang "(vim.api.nvim_get_current_buf())"))))

(fn on [event selector action]
  "Do `action` on autocmd `event`"
  (local fn-key (.. event "/" selector))
  (tset custom_autoloads fn-key action)
  (nvcmd (.. "autocmd " event " " selector " lua custom_autoloads[\"" fn-key "\"]()")))

{ : on-enter-lang
  : on
}
